package rest;

import dto.UserDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import services.UserService;

//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class UserControllerTest {

    @Configuration
    static class Context
    {
        @Bean
        UserService userService()
        {
            return new UserService();
        }
    }


    @Autowired
    private UserService userService;

    @Test
    public void createUser() throws Exception {

        UserDTO userNew = UserDTO.builder()
                .username("konrad")
                .email("example@com.pl")
                .password("hdu822d8*djw")
                .build();
        userService.createUser(userNew);
    }

    @Test
    public void getUser() throws Exception {
    }

    @Test
    public void updateUser() throws Exception {
    }

}