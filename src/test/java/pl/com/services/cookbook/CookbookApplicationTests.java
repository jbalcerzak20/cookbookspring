package pl.com.services.cookbook;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import rest.CategoryControllerTest;
import rest.UserControllerTest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CategoryControllerTest.class, UserControllerTest.class})
public class CookbookApplicationTests {

	@Test
	public void contextLoads() {
	}

}
