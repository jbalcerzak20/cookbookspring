package entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "Categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String name;

    @Column
    private LocalDateTime created_at = LocalDateTime.now();

    @Column
    @OneToMany(mappedBy = "category")
    private List<Rule> rules;


    public Category() {
    }

    public Category(String name) {
        this();
        this.name = name;
    }
}
