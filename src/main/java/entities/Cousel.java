package entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Cousels")
public class Cousel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column
    private String name;
    @Column
    private String description;

    public Cousel(){}
    public Cousel(String name, String description){
        this();
        this.name = name;
        this.description = description;
    }
}
