package entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "rule_component")
public class RuleComponent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "rule_id")
    private Rule rule;
    @ManyToOne
    @JoinColumn(name = "component_id")
    private Component component;

    @Column
    private String measure;
    @Column
    private Double value;


}
