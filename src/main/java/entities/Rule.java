package entities;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "Rules")
public class Rule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String name;

    @Column(length=65535)
    @Type(type = "text")
    private String rule;

    @Column
    private String difficulty;

    @Column
    private String operationTime;

    @Column
    private String personCount;

    @Column(length=65535)
    @Type(type = "text")
    private String picture;

    @Column
    private LocalDateTime created_at;

    @Column
    private LocalDateTime updated_at;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column
    @OneToMany(mappedBy = "rule")
    private List<Comment> comments;

    @Column
    private Boolean isPrivate;
}
