package entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column
    private String login;
    @Column
    private String password;
    @Column
    private String email;
    @Column
    private LocalDateTime created_at = LocalDateTime.now();
    @Column
    private LocalDateTime updated_at;
    @Column
    @OneToMany(mappedBy = "owner")
    private List<Rule> rules;

    @Column
    @OneToMany(mappedBy = "owner")
    private List<Comment> comments;
}
