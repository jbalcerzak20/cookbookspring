package rest;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import services.FileService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/rest/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void saveFile(@RequestParam("user-file") MultipartFile multipartFile, @RequestParam("id") Integer id) {
        fileService.saveFile(multipartFile, id);
    }

    @RequestMapping(value = "/get/{name}", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse httpServletResponse, @PathVariable("name") String name) {
        httpServletResponse.addHeader("Content-disposition", "attachment;filename=" + name);
        httpServletResponse.setContentType("text/plain");
        try {
            IOUtils.copy(fileService.getStreamOfFile(name), httpServletResponse.getOutputStream());
            httpServletResponse.flushBuffer();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
