package rest;

import dao.rule.RuleDao;
import dto.RuleDTO;
import entities.Category;
import entities.Component;
import entities.Rule;
import entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.RuleService;

import java.util.List;

@RestController
@RequestMapping("/rule")
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Rule> getRulesAll() {
        return ruleService.getRulesAll();
    }

    @RequestMapping(value = "/rulesByCategory/{categoryId}", method = RequestMethod.GET)
    public List<RuleDTO> getRulesByCategory(@PathVariable Integer categoryId) throws Exception {
        List<RuleDTO> rulesByCategory = ruleService.getRulesByCategory(categoryId);
        System.out.println(rulesByCategory);
        return rulesByCategory;
    }

    @RequestMapping(value = "/rulesByComponent", method = RequestMethod.POST)
    public List<RuleDTO> getRuleByComponents(@RequestBody List<Integer> components) {
        return ruleService.getRuleByComponents(components);
    }

    @RequestMapping(value = "/rulesbyuser", method = RequestMethod.POST)
    public List<Rule> getRulesByUser(@RequestBody User user) {
        return ruleService.getRulesByUser(user);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public RuleDTO createRule(@RequestBody RuleDTO rule) {
        return ruleService.createRule(rule);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateRule(@RequestBody RuleDTO rule) {
        ruleService.updateRule(rule);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void deleteRule(@RequestBody Rule rule) {
        ruleService.deleteRule(rule);
    }
}
