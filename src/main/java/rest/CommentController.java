package rest;

import dto.CommentDTO;
import entities.Comment;
import entities.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.CommentService;

import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createComment(@RequestBody CommentDTO comment) {
        commentService.createComment(comment);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public void editComment(@RequestBody Comment comment) {
        commentService.editComment(comment);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteComment(@RequestBody Comment comment) {
        commentService.deleteComment(comment);
    }

    @RequestMapping(value = "/getCommentByRule/{ruleId}", method = RequestMethod.GET)
    public List<CommentDTO> getCommentByRule(@PathVariable Integer ruleId) {
        return commentService.getCommentByRule(ruleId);
    }
}
