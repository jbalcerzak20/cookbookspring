package rest;

import entities.Cousel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.CouselService;

import java.util.List;

@RestController
@RequestMapping("/cousel")
public class CouselController {

    @Autowired
    private CouselService couselService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Cousel> getCouselAll() throws Exception {
        return couselService.getCouselAll();
    }

    @RequestMapping(value = "/{couselId}", method = RequestMethod.GET)
    public Cousel getCouselById(@PathVariable Integer couselId) throws Exception {
        return couselService.getCouselById(couselId);
    }

}
