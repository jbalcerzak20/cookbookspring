package rest;

import dto.CategoryDTO;
import entities.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.CategoryService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<CategoryDTO> getAllCategory() {
        return categoryService.getAllCategory();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createCategory(@RequestBody Category category) {
        categoryService.createCategory(category);
    }

}
