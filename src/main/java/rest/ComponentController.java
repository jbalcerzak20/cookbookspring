package rest;

import dto.ComponentDTO;
import dto.ComponentFullDTO;
import entities.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.ComponentService;

import java.util.List;

@RestController
@RequestMapping("/component")
public class ComponentController {

    @Autowired
    private ComponentService componentService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Component> getComponentAll() {
        return componentService.getComponentsAll();
    }

    @RequestMapping(value = "/componentForRule/{ruleId}", method = RequestMethod.GET)
    public List<ComponentFullDTO> getComponentByRule(@PathVariable Integer ruleId) throws Exception {
        return componentService.getComponentByRule(ruleId);
    }
}
