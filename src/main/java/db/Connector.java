package db;

import lombok.Data;
import lombok.Getter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Statement;

public class Connector {

    private static Connector ourInstance = new Connector();
    private EntityManagerFactory entityManagerFactory;
    @Getter
    private EntityManager entityManager;
    private Statement statement;

    public static Connector getInstance() {
        return ourInstance;
    }

    private Connector() {

        entityManagerFactory = Persistence.createEntityManagerFactory("Cook");
        entityManager = entityManagerFactory.createEntityManager();
    }
}
