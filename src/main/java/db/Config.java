package db;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import javax.persistence.EntityManager;

@Configuration
public class Config {
//    @Bean
//    @Primary
//    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
//        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
//        return builder.modulesToInstall(new JavaTimeModule());
//    }

    @Bean
    @Primary
    public EntityManager getEntityManager()
    {
        return Connector.getInstance().getEntityManager();
    }
}
