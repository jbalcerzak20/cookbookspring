import db.Connector;
import mock.CreateObjects;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"rest", "services", "dao", "filter","db"})

public class CookbookApplication {
    public static void main(String[] args) {
        SpringApplication.run(CookbookApplication.class, args);
        new CreateObjects().create(Connector.getInstance().getEntityManager());
    }
}
