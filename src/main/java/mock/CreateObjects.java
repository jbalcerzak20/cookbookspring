package mock;

import entities.*;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CreateObjects {

    private EntityManager entityManager;

    public void create(EntityManager entityManager) {
        this.entityManager = entityManager;

        Role adminRole = new Role();
        adminRole.setName("Admin");

        Role userRole = new Role();
        userRole.setName("User");

        User user = new User();
        user.setLogin("jacek");
        user.setEmail("jacek");
        user.setCreated_at(LocalDateTime.now());
        user.setUpdated_at(LocalDateTime.now());
        user.setPassword("123");

        List<Category> categories = new ArrayList<>();
        categories.add(buildCategory("Ciasta"));
        categories.add(buildCategory("Zupy"));
        categories.add(buildCategory("Sałatki"));
        categories.add(buildCategory("Ryby"));
        categories.add(buildCategory("Desery"));

        List<Component> components = new ArrayList<>();
        components.add(buildComponent("Masło"));
        components.add(buildComponent("Woda"));
        components.add(buildComponent("Mąka"));
        components.add(buildComponent("Jajko"));
        components.add(buildComponent("Proszek do pieczenia"));
        components.add(buildComponent("Budyń waniliowy"));
        components.add(buildComponent("Cukier"));
        components.add(buildComponent("Mleko"));
        components.add(buildComponent("Cukier puder"));
        components.add(buildComponent("Pomidory"));
        components.add(buildComponent("Ogórek"));
        components.add(buildComponent("Banan"));
        components.add(buildComponent("Jabłka"));

        Rule rule = buildRule(categories.get(0), "Karpatka", user,
                "30 min", "Latwe", "karpatka_0.jpg", "4-6");

        Rule rule2 = buildRule(categories.get(0), "Szarlotka", user,
                "30 min", "Latwe", "szarlotka_35.jpg", "4-6");

        List<RuleComponent> ruleComponents = new ArrayList<>();
        ruleComponents.add(buildRuleComponent(rule,components.get(0),"dkg",2.0));
        ruleComponents.add(buildRuleComponent(rule2,components.get(5),"sztuk",1.0));
        ruleComponents.add(buildRuleComponent(rule,components.get(2),"l",2.3));
        ruleComponents.add(buildRuleComponent(rule2,components.get(2),"g",0.4));
        ruleComponents.add(buildRuleComponent(rule2,components.get(4),"kg",11.0));

        entityManager.getTransaction().begin();
        entityManager.persist(user);

        categories.stream().forEach(cat -> entityManager.persist(cat));
        components.stream().forEach(com -> entityManager.persist(com));
        ruleComponents.stream().forEach(rul -> entityManager.persist(rul));
        buildCousels().stream().forEach(cou -> entityManager.persist(cou));

        entityManager.persist(rule);
        entityManager.persist(rule2);
        entityManager.getTransaction().commit();
    }

    public Category buildCategory(String name) {
        return new Category(name);
    }

    public Component buildComponent(String name) {
        return new Component(name);
    }

    public Rule buildRule(Category category, String name, User user, String time, String difficulty, String picture, String person) {
        Rule rule = new Rule();
        rule.setCategory(category);
        rule.setName(name);
        rule.setOwner(user);
        rule.setCreated_at(LocalDateTime.now());
        rule.setUpdated_at(LocalDateTime.now());
        rule.setOperationTime(time);
        rule.setDifficulty(difficulty);
        rule.setPersonCount(person);
        rule.setPicture(picture);
        rule.setIsPrivate(false);
        rule.setRule("Wszystkie składniki należy wymieszać");
        return rule;
    }

    public RuleComponent buildRuleComponent(Rule rule, Component component, String measure, Double value)
    {
        RuleComponent ruleComponent = new RuleComponent();
        ruleComponent.setComponent(component);
        ruleComponent.setRule(rule);
        ruleComponent.setMeasure(measure);
        ruleComponent.setValue(value);
        return ruleComponent;
    }

    public List<Cousel> buildCousels(){
        List<Cousel> couselList = new ArrayList<>();
        couselList.add(new Cousel("Porada numer 1","Opis porady numer 1"));
        couselList.add(new Cousel("Porada numer 2","Opis porady numer 2"));
        couselList.add(new Cousel("Porada numer 3","Opis porady numer 3"));
        couselList.add(new Cousel("Porada numer 4","Opis porady numer 4"));
        couselList.add(new Cousel("Porada numer 5","Opis porady numer 5"));
        return couselList;
    }
}
