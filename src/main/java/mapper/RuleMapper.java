package mapper;

import dto.RuleDTO;
import entities.Rule;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class RuleMapper {

    public static RuleDTO ruleToDto(Rule rule) {
        return RuleDTO.builder()
                .id(rule.getId())
                .category(rule.getCategory().getId())
                .owner(rule.getOwner().getId())
                .created_at(rule.getCreated_at() != null ? rule.getCreated_at().toString() : null)
                .updated_at(rule.getUpdated_at() != null ? rule.getUpdated_at().toString() : null)
                .difficulty(rule.getDifficulty())
                .isPrivate(rule.getIsPrivate())
                .name(rule.getName())
                .operationTime(rule.getOperationTime())
                .personCount(rule.getPersonCount())
                .pictureName(rule.getPicture())
                .rule(rule.getRule())
                .build();
    }

    public static Rule dtoToRule(RuleDTO ruleDTO) {
        Rule rule = new Rule();
        rule.setId(ruleDTO.getId());
        rule.setRule(ruleDTO.getRule());
        rule.setIsPrivate(ruleDTO.getIsPrivate());
        rule.setPersonCount(ruleDTO.getPersonCount());
        rule.setPicture(ruleDTO.getPictureName());
        rule.setDifficulty(ruleDTO.getDifficulty());
        rule.setOperationTime(ruleDTO.getOperationTime());
        rule.setCreated_at(ruleDTO.getCreated_at() != null ? LocalDateTime.ofInstant(Instant.ofEpochMilli(new Long(ruleDTO.getCreated_at())), ZoneId.systemDefault()) : null);
        rule.setUpdated_at(ruleDTO.getUpdated_at() != null ? LocalDateTime.ofInstant(Instant.ofEpochMilli(new Long(ruleDTO.getUpdated_at())), ZoneId.systemDefault()) : null);
        rule.setName(ruleDTO.getName());
        return rule;
    }
}
