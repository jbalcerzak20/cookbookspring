package mapper;

import dto.CommentDTO;
import entities.Comment;

import java.time.LocalDateTime;

public class CommentMapper {

    public static CommentDTO commentToDto(Comment comment) {
        return CommentDTO.builder()
                .id(comment.getId())
                .message(comment.getMessage())
                .title(comment.getTitle())
                .owner(comment.getOwner().getId())
                .rule(comment.getRule().getId())
                .login(comment.getOwner().getLogin())
                .created_at(comment.getCreated_at() != null ? comment.getCreated_at().toString() : null)
                .updated_at(comment.getUpdated_at() != null ? comment.getUpdated_at().toString() : null)
                .build();
    }

    public static Comment dtoToComment(CommentDTO commentDTO) {
        Comment comment = new Comment();
        comment.setId(commentDTO.getId());
        comment.setMessage(commentDTO.getMessage());
        comment.setTitle(commentDTO.getTitle());
        comment.setOwner(null);
        comment.setRule(null);
        comment.setCreated_at(commentDTO.getCreated_at() != null ? LocalDateTime.parse(commentDTO.getCreated_at()) : null);
        comment.setUpdated_at(commentDTO.getUpdated_at() != null ? LocalDateTime.parse(commentDTO.getUpdated_at()) : null);
        return comment;
    }
}
