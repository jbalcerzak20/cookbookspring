package mapper;

import dto.CategoryDTO;
import entities.Category;

import java.time.LocalDateTime;

public class CategoryMapper {

    public static CategoryDTO categoryToDto(Category category) {
        return CategoryDTO.builder()
                .id(category.getId())
                .name(category.getName())
                .created_at(category.getCreated_at() != null ? category.getCreated_at().toString() : null)
                .build();
    }

    public static Category dtoToCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setName(categoryDTO.getName());
        category.setId(categoryDTO.getId());
        category.setCreated_at(categoryDTO.getCreated_at() != null ? LocalDateTime.parse(categoryDTO.getCreated_at()) : null);
        return category;
    }
}
