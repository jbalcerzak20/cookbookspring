package mapper;

import dto.ComponentDTO;
import entities.Component;

public class ComponentMapper {

    public static ComponentDTO componentToDto(Component component) {
        return ComponentDTO.builder()
                .id(component.getId())
                .name(component.getName())
                .build();
    }

    public static Component dtoToComponent(ComponentDTO componentDTO) {
        Component component = new Component();
        component.setName(componentDTO.getName());
        component.setId(componentDTO.getId());
        return component;
    }
}
