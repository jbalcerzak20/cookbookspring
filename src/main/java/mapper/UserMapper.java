package mapper;

import dto.UserDTO;
import entities.User;

import java.time.LocalDateTime;

public class UserMapper {

    public static UserDTO userToDto(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .username(user.getLogin())
                .password(user.getPassword())
                .email(user.getEmail())
                .created_at(user.getCreated_at()!=null?user.getCreated_at().toString():null)
                .created_at(user.getUpdated_at()!=null?user.getUpdated_at().toString():null)
                .build();
    }

    public static User dtoToUser(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setLogin(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        if (userDTO.getCreated_at() != null) {
            user.setCreated_at(LocalDateTime.parse(userDTO.getCreated_at()));
        }
        if (userDTO.getUpdated_at() != null) {
            user.setUpdated_at(LocalDateTime.parse(userDTO.getUpdated_at()));
        }
        return user;
    }
}
