package filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import dto.UserDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.GenericFilterBean;
import services.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;


@EnableWebSecurity
@Configuration
@PropertySource("application.properties")
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${secure.server.password}")
    private String SECURE_PASSWORD;
    @Value("${secure.expiration.day}")
    private long EXPIRATION_DAY;

    @Autowired
    private UserService userService;
    private entities.User userByEmail;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers(HttpMethod.DELETE).authenticated()
                .antMatchers(HttpMethod.PUT).authenticated()
//                .antMatchers(HttpMethod.POST).authenticated()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/api/user/create").permitAll()
                .antMatchers(HttpMethod.POST,"/**/search").permitAll()
//                .anyRequest().authenticated()
                .and()
                .addFilterBefore(new JWTTokenCreator("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        http.httpBasic().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .passwordEncoder(passwordEncoder())
//                .withUser("user")
//                .password("password")
//                .roles("USER");

        auth.userDetailsService(s -> {
            userByEmail = userService.findUserByEmail(s);
            return new org.springframework.security.core.userdetails.User(userByEmail.getEmail(), userByEmail.getPassword(), Collections.emptyList());
        });
    }

    @SuppressWarnings("deprecation")
    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }

    private class JWTAuthenticationFilter extends GenericFilterBean {
        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

            Collection<GrantedAuthority> collection = new ArrayList<>();
            String token = ((HttpServletRequest) servletRequest).getHeader("Authorization");

            Authentication auth = null;

            if (token != null) {
                try {
                    String user = Jwts.parser()
                            .setSigningKey(SECURE_PASSWORD.getBytes("UTF-8"))
                            .parseClaimsJws(token)
                            .getBody().getSubject();
                    if (user != null)
                        auth = new UsernamePasswordAuthenticationToken(user, null, collection);
                } catch (SignatureException e) {
                    logger.error(e);
                }
            }
            SecurityContextHolder.getContext().setAuthentication(auth);
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private class JWTTokenCreator extends AbstractAuthenticationProcessingFilter {

        protected JWTTokenCreator(String url, AuthenticationManager authenticationManager) {
            super(new AntPathRequestMatcher(url));
            setAuthenticationManager(authenticationManager);
        }

        @Override
        public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
                throws AuthenticationException, IOException, ServletException {

            User user = new ObjectMapper().readValue(httpServletRequest.getInputStream(), User.class);
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), Collections.emptyList());
            return getAuthenticationManager().authenticate(auth);
        }

        @Override
        protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
                throws IOException, ServletException {

            String token = Jwts
                    .builder()
                    .setSubject("konrad")
                    .setExpiration(new Date(LocalDate.now().plusDays(EXPIRATION_DAY).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli()))
                    .signWith(SignatureAlgorithm.HS256, SECURE_PASSWORD.getBytes("UTF-8"))
                    .compact();

            response.addHeader("Authorization", "Bearer " + token);
            response.addHeader("Content-Type", "application/json");


            UserDTO userDTO = UserMapper.userToDto(userByEmail);
            userDTO.setToken(token);
            response.getWriter().print(new Gson().toJson(userDTO));
        }
    }
}

