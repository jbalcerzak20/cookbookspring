package dao.category.impl;

import dao.category.CategoryDao;
import db.Connector;
import entities.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Primary
public class CategoryDaoImpl implements CategoryDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Category> getAllCategory() {
        return entityManager.createQuery("From Category c", Category.class).getResultList();
    }

    @Override
    public void createCategory(Category category) {
        category.setCreated_at(LocalDateTime.now());
        entityManager.getTransaction().begin();
        entityManager.persist(category);
        entityManager.getTransaction().commit();
    }
}
