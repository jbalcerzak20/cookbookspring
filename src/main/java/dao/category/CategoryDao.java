package dao.category;

import entities.Category;

import java.util.List;

public interface CategoryDao {

    List<Category> getAllCategory();

    void createCategory(Category category);

}
