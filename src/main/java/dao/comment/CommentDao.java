package dao.comment;

import entities.Comment;
import entities.Rule;

import java.util.List;

public interface CommentDao {

    void createComment(Comment comment);

    void updateComment(Comment comment);

    void deleteComment(Comment comment);

    List<Comment> getCommentAll();

    List<Comment> getCommentByRule(Integer ruleId);
}
