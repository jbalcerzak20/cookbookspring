package dao.comment.impl;

import dao.comment.CommentDao;
import db.Connector;
import entities.Comment;
import entities.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;

@Primary
@Service
public class CommentDaoImpl implements CommentDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void createComment(Comment comment) {
        entityManager.getTransaction().begin();
        comment.setCreated_at(LocalDateTime.now());
        entityManager.persist(comment);
        entityManager.getTransaction().commit();
    }

    @Override
    public void updateComment(Comment comment) {
        entityManager.getTransaction().begin();
        entityManager.merge(comment);
        entityManager.getTransaction().commit();
    }

    @Override
    public void deleteComment(Comment comment) {
        entityManager.getTransaction().begin();
        entityManager.remove(comment);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Comment> getCommentAll() {
        return entityManager.createQuery("From Comment c").getResultList();
    }

    @Override
    public List<Comment> getCommentByRule(Integer ruleId) {
        Query query = entityManager.createQuery("from Comment c where c.rule.id =:rule order by c.created_at desc");
        query.setParameter("rule", ruleId);
        return query.getResultList();
    }
}
