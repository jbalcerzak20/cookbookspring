package dao.component.impl;

import dao.component.ComponentDao;
import dto.ComponentFullDTO;
import entities.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Primary
@Service
public class ComponentDaoImpl implements ComponentDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void createComponent(Component component) {
        entityManager.getTransaction().begin();
        entityManager.persist(component);
        entityManager.getTransaction().commit();
    }

    @Override
    public void updateComponent(Component component) {
        entityManager.getTransaction().begin();
        entityManager.merge(component);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Component> getAll() {
        return entityManager.createQuery("From Component c").getResultList();
    }

    @Override
    public void deleteComponent(Component component) {
        entityManager.getTransaction().begin();
        entityManager.remove(component);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<ComponentFullDTO> getComponentByRule(Integer ruleId) {

        StringBuilder builder = new StringBuilder();
        builder.append("select c.* from rules_componets rc" +
                " join components c on rc.component_id = c.id\n" +
                "where rc.rule_id = " +ruleId);

        TypedQuery<ComponentFullDTO> query1 = entityManager.createQuery("Select new dto.ComponentFullDTO(c.id,c.name,rc.measure,rc.value) From RuleComponent rc left join Component c on c.id = rc.component.id where rc.rule.id = " + ruleId, ComponentFullDTO.class);

//        Query query = entityManager.createNativeQuery(builder.toString(), Component.class);
//        return query.getResultList();
        return query1.getResultList();
    }
}
