package dao.component;

import dto.ComponentFullDTO;
import entities.Component;
import entities.Rule;

import java.util.List;

public interface ComponentDao {
    void createComponent(Component component);

    void updateComponent(Component component);

    List<Component> getAll();

    void deleteComponent(Component component);

    List<ComponentFullDTO> getComponentByRule(Integer ruleId);
}
