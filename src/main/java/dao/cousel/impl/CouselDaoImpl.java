package dao.cousel.impl;

import dao.cousel.CouselDao;
import entities.Cousel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Primary
@Service
public class CouselDaoImpl implements CouselDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Cousel> getAllCousel() throws Exception {
        return entityManager.createQuery("From Cousel").getResultList();
    }

    @Override
    public Cousel getCouselById(Integer id) throws Exception {
        return entityManager.createQuery("From Cousel c where c.id =" + id, Cousel.class).getSingleResult();
    }
}
