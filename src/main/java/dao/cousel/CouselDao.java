package dao.cousel;

import entities.Cousel;

import java.util.List;

public interface CouselDao {
    List<Cousel> getAllCousel() throws Exception;
    Cousel getCouselById(Integer id) throws Exception;
}
