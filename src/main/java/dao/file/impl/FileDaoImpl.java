package dao.file.impl;

import dao.file.FileDao;
import dao.rule.RuleDao;
import entities.Rule;
import mapper.RuleMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
@Primary
public class FileDaoImpl implements FileDao {

    private String directory = "./storage/";
    @Autowired
    private RuleDao ruleDao;

    @Override
    public void saveFile(MultipartFile multipartFile, Integer idRule) {
        String originalFilename = multipartFile.getOriginalFilename();

        File dir = new File(directory);
        if (!dir.exists()) {
            dir.mkdir();
        }

        Rule ruleById = ruleDao.getRuleById(idRule);

        RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a', 'z').build();
        String newFileName = generator.generate(8) + originalFilename;

        try {
            FileUtils.writeByteArrayToFile(new File(directory + newFileName), multipartFile.getBytes());
            ruleById.setPicture(newFileName);
            ruleDao.updateRule(RuleMapper.ruleToDto(ruleById));
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    public File getFileFromServer(String fileName) {
        return new File(directory + fileName);
    }
}
