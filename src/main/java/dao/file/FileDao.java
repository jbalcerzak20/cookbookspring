package dao.file;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface FileDao {

    void saveFile(MultipartFile multipartFile, Integer idRule);

    File getFileFromServer(String fileName);

}
