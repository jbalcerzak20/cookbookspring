package dao.rule;

import dto.RuleDTO;
import entities.Category;
import entities.Component;
import entities.Rule;
import entities.User;

import java.util.List;

public interface RuleDao {

    List<Rule> getRulesByCategory(Integer categoryId) throws Exception;

    List<Rule> getRulesByUser(User user);

    List<Rule> getRulesAll();

    List<Rule> getRuleByComponents(List<Integer> components);

    Rule getRuleById(Integer id);

    Rule createRule(Rule rule);

    void deleteRule(Rule rule);

    void updateRule(RuleDTO rule);
}
