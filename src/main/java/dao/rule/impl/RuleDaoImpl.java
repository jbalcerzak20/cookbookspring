package dao.rule.impl;

import dao.rule.RuleDao;
import db.Connector;
import dto.ComponentFullDTO;
import dto.RuleDTO;
import entities.*;
import mapper.RuleMapper;
import org.hibernate.query.criteria.internal.CriteriaDeleteImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaDelete;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Primary
public class RuleDaoImpl implements RuleDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Rule> getRulesByCategory(Integer categoryId) {
        Query query = entityManager.createQuery("From Rule r where r.category.id =:category");
        query.setParameter("category", categoryId);
        return query.getResultList();
    }

    @Override
    public List<Rule> getRulesByUser(User user) {
        Query query = entityManager.createQuery("From Rule r where r.owner =:owner");
        query.setParameter("owner", user);
        return query.getResultList();
    }

    @Override
    public List<Rule> getRulesAll() {
        return entityManager.createQuery("From Rule r").getResultList();
    }

    @Override
    public List<Rule> getRuleByComponents(List<Integer> components) {


        StringBuilder ids = new StringBuilder();
        StringBuilder finalIds = ids;
        components.stream().forEach(id -> finalIds.append(","+id));
        ids = finalIds;
        ids = ids.replace(0,1,"");

//        select * from rules r
//        where r.id in (select rc.rule_id from rule_component rc
//        where rc.component_id in (11,13)
//        group by rc.rule_id
//        having count(rc.rule_id)>=2);


        StringBuilder builder = new StringBuilder();
        builder.append("select * from rules r ");
        builder.append("where r.id in (select rc.rule_id from rule_component rc ");
        builder.append("where rc.component_id in ("+ids.toString()+") ");
        builder.append("group by rc.rule_id ");
        builder.append("having count(rc.rule_id)>="+components.size()+")");
        Query query = entityManager.createNativeQuery(builder.toString(),Rule.class);

        return query.getResultList();
    }

    @Override
    public Rule getRuleById(Integer id) {
        return entityManager.find(Rule.class, id);
    }

    @Override
    public Rule createRule(Rule rule) {
        rule.setCreated_at(LocalDateTime.now());
        entityManager.getTransaction().begin();
        entityManager.persist(rule);
        entityManager.flush();
        entityManager.getTransaction().commit();
        return rule;
    }

    @Override
    public void deleteRule(Rule rule) {
        entityManager.remove(rule);
    }

    @Override
    public void updateRule(RuleDTO rule) {

        List<ComponentFullDTO> components = rule.getComponents();
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from RuleComponent r where r.rule.id = "+rule.getId()).executeUpdate();

        Rule rule1 = RuleMapper.dtoToRule(rule);

        for (ComponentFullDTO comp: components) {
            RuleComponent ruleComponent = new RuleComponent();
            ruleComponent.setMeasure(comp.getMeasure());
            ruleComponent.setValue(comp.getValue());
            ruleComponent.setRule(entityManager.find(Rule.class,rule.getId()));
            ruleComponent.setComponent(entityManager.find(Component.class,comp.getId()));
            entityManager.persist(ruleComponent);
        }
        rule1.setUpdated_at(LocalDateTime.now());
        rule1.setOwner(entityManager.find(User.class, rule.getOwner()));
        rule1.setCategory(entityManager.find(Category.class, rule.getCategory()));
        entityManager.merge(rule1);
        entityManager.getTransaction().commit();
    }
}
