package dao.user.impl;

import dao.user.UserDao;
import db.Connector;
import entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@Primary
public class UserDaoImpl implements UserDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void createUser(User user) throws Exception {
        user.setCreated_at(LocalDateTime.now());
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public User getUser(Integer id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void deleteUser(Integer id) {
        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.find(User.class, id));
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void updateUser(User user) {
        user.setUpdated_at(LocalDateTime.now());
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public User login(User user) {
        Query namedQuery = entityManager.createQuery("From User u where u.email = :email and u.password = :password");
        namedQuery.setParameter("email", user.getEmail());
        namedQuery.setParameter("password", user.getPassword());
        return (User) namedQuery.getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public User userByUsername(String username) {
        Query namedQuery = entityManager.createQuery("From User u where u.email = :email");
        namedQuery.setParameter("email", username);
        return (User) namedQuery.getResultList().stream().findFirst().orElse(null);
    }
}
