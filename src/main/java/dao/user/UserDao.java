package dao.user;

import entities.User;

public interface UserDao {

    void createUser(User user) throws Exception;

    User getUser(Integer id);

    void deleteUser(Integer id);

    void updateUser(User user);

    User login(User user);

    User userByUsername(String username);

}
