package services;

import dao.comment.CommentDao;
import dto.CommentDTO;
import entities.Comment;
import entities.Rule;
import entities.User;
import mapper.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    private CommentDao commentDao;
    @Autowired
    private UserService userService;
    @Autowired
    private RuleService ruleService;

    public void createComment(CommentDTO comment) {
        Comment cm = CommentMapper.dtoToComment(comment);

        User user = userService.getUser(comment.getOwner());
        Rule rule = ruleService.getRulesAll().stream().filter(rul -> rul.getId().equals(comment.getRule())).findFirst().orElse(null);

        cm.setRule(rule);
        cm.setOwner(user);
        commentDao.createComment(cm);
    }

    public void editComment(Comment comment) {
        commentDao.updateComment(comment);
    }

    public void deleteComment(Comment comment) {
        commentDao.deleteComment(comment);
    }

    public List<CommentDTO> getCommentByRule(Integer ruleId) {
        return commentDao.getCommentByRule(ruleId)
                .stream()
                .map(com -> CommentMapper.commentToDto(com))
                .collect(Collectors.toList());
    }
}
