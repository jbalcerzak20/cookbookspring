package services;

import dao.category.CategoryDao;
import dao.rule.RuleDao;
import dao.user.UserDao;
import dto.RuleDTO;
import entities.Rule;
import entities.User;
import mapper.RuleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RuleService {

    @Autowired
    private RuleDao ruleDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private UserDao userDao;

    public List<Rule> getRulesAll() {
        return ruleDao.getRulesAll();
    }

    public List<RuleDTO> getRulesByCategory(Integer categoryId) throws Exception {
        return ruleDao.getRulesByCategory(categoryId)
                .stream()
                .map(rule -> RuleMapper.ruleToDto(rule))
                .collect(Collectors.toList());
    }

    public List<Rule> getRulesByUser(User user) {
        return ruleDao.getRulesByUser(user);
    }

    public RuleDTO createRule(RuleDTO rule) {
        Rule rule1 = RuleMapper.dtoToRule(rule);
        rule1.setCategory(categoryDao.getAllCategory().stream().filter(cat -> cat.getId().equals(rule.getCategory())).findFirst().orElse(null));
        rule1.setOwner(userDao.getUser(rule.getOwner()));
        return RuleMapper.ruleToDto(ruleDao.createRule(rule1));
    }

    public void updateRule(RuleDTO rule) {
        ruleDao.updateRule(rule);
    }

    public void deleteRule(Rule rule) {
        ruleDao.deleteRule(rule);
    }

    public List<RuleDTO> getRuleByComponents(List<Integer> components) {
        return ruleDao.getRuleByComponents(components)
                .stream()
                .map(rule -> RuleMapper.ruleToDto(rule))
                .collect(Collectors.toList());
    }
}
