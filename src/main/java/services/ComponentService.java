package services;

import dao.component.ComponentDao;
import dto.ComponentDTO;
import dto.ComponentFullDTO;
import entities.Component;
import mapper.ComponentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ComponentService {

    @Autowired
    private ComponentDao componentDao;

    public List<Component> getComponentsAll() {
        return componentDao.getAll();
    }

    public List<ComponentFullDTO> getComponentByRule(Integer ruleId) {

        return componentDao.getComponentByRule(ruleId);
    }
}
