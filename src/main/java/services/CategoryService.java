package services;

import dao.category.CategoryDao;
import dto.CategoryDTO;
import entities.Category;
import mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    public List<CategoryDTO> getAllCategory() {
        return categoryDao.getAllCategory()
                .stream()
                .map(cat -> CategoryMapper.categoryToDto(cat))
                .collect(Collectors.toList());
    }

    public void createCategory(Category category) {
        categoryDao.createCategory(category);
    }

}
