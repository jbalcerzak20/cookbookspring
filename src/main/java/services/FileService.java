package services;

import dao.file.FileDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Service
public class FileService {

    @Autowired
    private FileDao fileDao;

    public void saveFile(MultipartFile multipartFile, Integer idRule) {
        fileDao.saveFile(multipartFile, idRule);
    }

    public InputStream getStreamOfFile(String fileName) throws FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(fileDao.getFileFromServer(fileName)));
    }

}
