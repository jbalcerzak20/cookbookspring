package services;

import dao.cousel.CouselDao;
import entities.Cousel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CouselService {

    @Autowired
    private CouselDao couselDao;

    public List<Cousel> getCouselAll() throws Exception {
        return couselDao.getAllCousel();
    }

    public Cousel getCouselById(Integer id) throws Exception {
        return couselDao.getCouselById(id);
    }
}
