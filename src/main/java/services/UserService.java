package services;

import dao.user.UserDao;
import dto.UserDTO;
import entities.User;
import mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public User getUser(Integer id) {
        return userDao.getUser(id);
    }

    public void createUser(UserDTO user) throws Exception{
       userDao.createUser(UserMapper.dtoToUser(user));
    }

    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    public void deleteUser(Integer id) {
        userDao.deleteUser(id);
    }

    public User login(User user) {
        return userDao.login(user);
    }

    public User findUserByEmail(String email) {
        return userDao.userByUsername(email);
    }

}
