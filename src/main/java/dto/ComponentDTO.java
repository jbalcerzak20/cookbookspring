package dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ComponentDTO {
    private Integer id;
    private String name;
}
