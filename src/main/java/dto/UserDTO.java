package dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class UserDTO {
    private Integer id;
    private String username;
    private String password;
    private String email;
    private String created_at;
    private String updated_at;
    private String token;
}
