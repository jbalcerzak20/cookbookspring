package dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class RuleDTO {
    private Integer id;
    private String name;
    private String rule;
    private String difficulty;
    private String operationTime;
    private String personCount;
    private String pictureName;
    private String created_at;
    private String updated_at;
    private Integer owner;
    private Integer category;
    private Boolean isPrivate;
    private List<ComponentFullDTO> components;

}
