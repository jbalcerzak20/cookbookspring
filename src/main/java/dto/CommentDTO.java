package dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommentDTO {
    private Integer id;
    private String title;
    private String message;
    private Integer owner;
    private Integer rule;
    private String created_at;
    private String updated_at;
    private String login;
}
