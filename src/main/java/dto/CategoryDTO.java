package dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CategoryDTO {
    private Integer id;
    private String name;
    private String created_at;
}
