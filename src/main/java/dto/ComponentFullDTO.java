package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ComponentFullDTO {
    private Integer id;
    private String name;
    private String measure;
    private Double value;
}
